
### Hey there <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="25px">

<img align="right" alt="GIF" src="https://camo.githubusercontent.com/97d1828fe16ccca3417229fc085cfc96062bd74c8787f80131ddc9462ce4ed51/68747470733a2f2f736f6669616e6568616d6c616f75692e6769746875622e696f2f6a756e6b2f6c6f636b646f6f722f6c6f676f732f6c6f676f323035783235302e676966?raw=true" width="400" height="250"/>




I'm **Akash Kumar**(<a href="https://github.com/Akash671/" target="_blank">Akash671</a>),i am a <a href="https://code.dcoder.tech/profile/akashsaini">competitive programmer</a> and machine learning engineer as a student with strong background in mathematics, statistics, and <a href="https://www.hackerrank.com/akashsaini454545">problem solving</a>.i am doing bachelor of technology(B.Tech) in computer science & engineering(cse) at <a href="https://www.mitmoradabad.edu.in/" target="_blank"> Moradabad Institute Of Technology Moradabad, Uttar Pradesh India</a>, with majors in technology and bachelor in science supported by<a href="https://aktu.ac.in/"> A.P.J Abdul Kalam Technical University(AKTU) </a>with majors in engineering and operational research.</a>

<br>
<br>
<br>
<br>
<img align="right" alt="GIF" src="https://cdn.dribbble.com/users/2344801/screenshots/4774578/alphatestersanimation2.gif?raw=true" width="450" height="250"/>
<br>
<br>

[Contacts]
<br>
<img align="left" alt="Akash's LinkdeIN" width="26px" src="https://image.flaticon.com/icons/png/512/174/174857.png"/> <a href="https://www.linkedin.com/in/akash-kumar-52563018a/">LinkedIn</a>
<br>
<br>
<img align="left" alt="Akash's Youtube" width="50px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/Logo_of_YouTube_%282015-2017%29.svg/1200px-Logo_of_YouTube_%282015-2017%29.svg.png"/> <a href="https://youtu.be/DgjB3GTsdao">Youtube</a>
<br><br>
<img align="left" alt="Akash's Medium" width="80px" src="https://miro.medium.com/max/8976/1*Ra88BZ-CSTovFS2ZSURBgg.png"/> <a href="https://medium.com/@akashsaininasa">Medium</a>
<br>
<br>

Others Competetive profile...
<br>
<a href="https://www.facebook.com/codingcompetitions/hacker-cup/">Facebook Hacker Cup</a>
<br>
<a href="https://www.topcoder.com/">Topcoder(Akash_1996)</a>
<br>
<a href="https://icpc.global/private/profile/584919">ICPC(International Collegiate Programming Contest)</a>
<br>
<a href="https://codingcompetitions.withgoogle.com/kickstart/certificate/summary/000000000019ffc6">Google Kick Start(Akash@1996)</a>
<br>
<a href="https://codingcompetitions.withgoogle.com/codejam/certificate/summary/00000000001857b3">Google CodeJam(Akash@1996)</a>
<br>
<a href="https://app.hackthebox.eu/profile/595653">Hack The Box(kal103)</a>
<br>
<a href="https://code.dcoder.tech/profile/akashsaini">Decoder(@akashsaini)</a>
<br>
<a href="https://nextstep.tcs.com/campus/#/CT20203113369">TCS CodeVita</a>
<br>
<a href="https://stackoverflow.com/users/14312178/akash-kumar?tab=profile/">Stackoverflow</a>
<br>
<a href="https://www.facebook.com/profile.php?id=100034209998322">Facebook</a>
<br>
<a href="https://my.newtonschool.co/user/akashsainimit/">Newton School(akashsainimit)</a>
<br>
<a href="https://leetcode.com/Akashmit1996/">leetcode(Akashmit1996)</a>
<br>
<a href="https://auth.geeksforgeeks.org/user/hitmanmit1996/practice/">Geeksforgeeks(hitmanmit1996)</a>
<br>
<a href="https://www.hackerrank.com/akashsaini454545">Hackerrank(akashsaini454545)</a>
<br>
<a href="http://www.hackerearth.com/@akash4031">Hackerearth(akash4031)</a>
<br>
<a href="https://www.codechef.com/users/akash1996kumar">Codechef(akash1996kumar)</a>
<br>
<a href="https://projecteuler.net/profile/Akash1996.png">Project Euler(Akash1996)</a>
<br>
<a href="https://gitlab.com/Akash671">GitLab(Akash671)</a>
<br>
<a href="http://codeforces.com/profile/akashsaini454545">CodeForce(akashsaini454545)</a>
<br>
<a href="https://www.codewars.com/users/Akash671">Codewar(Akash671)</a>
<br>
<a href="https://www.coderbyte.com/profile/Akash1996">Codebyte(Akash1996)</a>
<br>
<a href="https://atcoder.jp/users/Akash_1996">Atcoder(Akash_1996)</a>
<br>
<a href="https://hack.codingblocks.com/app/users/259785">CodeBlock</a>
<br>
<a href="https://profile.codingninjas.com/6b366c45-0661-4bd6-912b-aa18e9d7783e?_ga=2.120932736.940989445.1619538278-171848948.1616765825">Coding Ninjas</a>
<br>
<a href="https://githubmemory.com/@Akash671">About me</a>



      Mostly use languages..
<a href="https://github.com/Akash671">
<img align="center" alt="Akash's Github Stats" src="https://github-readme-stats.codestackr.vercel.app/api?username=Akash671&show_icons=true&hide_border=true&count_private=true&include_all_commits=true&theme=radical" /></a>      <a href="https://github.com/Akash671">
  <img align="center" src="https://github-readme-stats.anuraghazra1.vercel.app/api/top-langs/?username=Akash671&layout=compact&theme=radical&langs_count=10" />
</a>            
 

Profile Views

![Visitor Count](https://profile-counter.glitch.me/{Akash671}/count.svg)

<p align="left"> 
  <img src="https://komarev.com/ghpvc/?username=Akash671&label=PROFILE+VIEWS" alt="Akash671"/> 
</p>

<p align="left">	  
 <a href="https://Akash671.github.io/"><img src="https://hits.seeyoufarm.com/api/count/incr/badge.svg?url=https%3A%2F%2FAkash671.github.io&count_bg=%2379C83D&title_bg=%23555555&icon=&icon_color=%23E7E7E7&title=HOME+PAGE+VIEWS&edge_flat=false"/></a>
</p>	

[<img alt="Youtube" src="https://img.shields.io/badge/Youtube%20-%23FF0000.svg?&style=for-the-badge&logo=YouTube&logoColor=white"/>](https://youtu.be/DgjB3GTsdao) 
[<img alt="LinkedIn" src="https://img.shields.io/badge/linkedin%20-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white"/>](https://www.linkedin.com/in/akash-kumar-52563018a/) 
[<img alt="Gmail" src="https://img.shields.io/badge/@hitmanmit1996@GMAİL.COM-D14836?style=for-the-badge&logo=gmail&logoColor=white" />](hitmanmit1996@gmail.com)


# Featured Contents

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=Akash671&repo=ML-Project&theme=tokyonight)](https://github.com/Akash671/ML-Project)[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=Akash671&repo=Algorithms&theme=tokyonight)](https://github.com/Akash671/Algorithms)

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=Akash671&repo=KickStart&theme=tokyonight)](https://github.com/Akash671/KickStart)[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=Akash671&repo=geeksforgeeks&theme=tokyonight)](https://github.com/Akash671/geeksforgeeks)

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=Akash671&repo=linux_script&theme=tokyonight)](https://github.com/Akash671/linux_script)[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=Akash671&repo=Codechef&theme=tokyonight)](https://github.com/Akash671/Codechef)

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=Akash671&repo=AI&theme=tokyonight)](https://github.com/Akash671/AI)[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=Akash671&repo=C_C-&theme=tokyonight)](https://github.com/Akash671/C_C-)
<br>
<br>
<img alt="GIF" src="https://giffiles.alphacoders.com/578/57857.gif?raw=true" width="550" height="260"/> <img alt="GIF" src="https://i.imgur.com/uUgLY.gif?raw=true" width="260" height="260"/>
<br>
About me
<br>
<img align="left" alt="GIF" src="https://github.com/Akash671/About_me/blob/main/hck.png?raw=true" width="400" height="240"/> <img align="center" alt="sGIF" src="https://github.com/Akash671/About_me/blob/main/gfg.png?raw=true" width="400" height="240"/>
<br>
<br>
<img align="left" alt="GIF" src="https://github.com/Akash671/About_me/blob/main/decoder.png?raw=true" width="400" height="240"/><img align="center" alt="GIF" src="https://github.com/Akash671/About_me/blob/main/google.jpg?raw=true" width="400" height="240"/>
<br>
